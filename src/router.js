import Vue from 'vue';
import VueRouter from "vue-router";

Vue.use( VueRouter );
/* import Playground from "./components/playground/Playground";
import Home from "./routes/home/components/Home/Home";
import Page404 from "./components/Page404/Page404"; */


/* const Playground = r => require.ensure( [], () => r( require( './components/playground/Playground' ) ), 'play' );
const Home = r => require.ensure( [], () => r( require( './routes/home/components/Home/Home' ) ), 'home' );
const Page404 = r => require.ensure( [], () => r( require( './components/Page404/Page404' ) ), 'page404' );
 */
const Playground = () => import( './components/playground/Playground' );
const Home = () => import( './routes/home/components/Home/Home' );
const Page404 = () => import( './components/Page404/Page404' );


const router = new VueRouter( {
    mode: 'history',
    routes: [
        {
            path: '/play/:id',
            component: Playground,
            meta: {
                title: 'Playground'
            }
        },
        {
            path: '/',
            component: Home,
            meta: {
                title: 'Home'
            }
        },
        {
            path: '*',
            component: Page404,
            meta: {
                title: '404'
            }
        }
    ]
} );

router.beforeEach(( to, from, next ) => {
    document.title = to.meta.title
    next()
} )

export default router;