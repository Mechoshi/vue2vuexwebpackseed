export default {
    addUser( context, payload ) { //{commit} - deconstructing context
        setTimeout(
            () => {
                context.commit( 'addUser', payload );
            },
            2000
        );
    }
}