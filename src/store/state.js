export default {
    greeting: 'Hi',
    clicks: 0,
    message: 'From parent',
    personId: 0,
    persons: [
        {
            name: 'Miecho',
            age: 40
        },
        {
            name: 'Silche',
            age: 33
        }
    ]
}